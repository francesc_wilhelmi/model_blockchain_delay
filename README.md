# README #

This repository contains an implementation of the batch service queue model proposed in:

```Wilhelmi, F., & Giupponi, L. (2021). Discrete-Time Analysis of Wireless Blockchain Networks. arXiv preprint arXiv:2104.05586.```

The code is written in Matlab.

### Authors ###

* Lorenza Giupponi (lorenza.giupponi@cttc.es)
* Francesc Wilhelmi (fwilhelmi@cttc.cat)

### Repository description ###

The batch-service queue model implementation allows computing the queue delay and the queue occupancy of a blockchain-based system with timers and forks. The parameters that can be configured are as follows:

1. **lambda:** total system arrivals [pkt/s]
2. **mu:** service rate [batch per second]
3. **queue_size:** maximum size of the queue [# of packets]
4. **block_size:** minimum block size to serve batches [# of packets]
5. **timer:** maximum waiting time before serving a batch [seconds]
6. **n_fork:** proportion of forks that occur when serving a batch (depends on the number of concurrent miners and the block propagation delay)

The main model engine can be found at subfolder './Matlab code/Model', while the rest of folders (e.g., './Matlab code/Methods/Deployment', './Matlab code/Methods/Bianchi', './Matlab code/Methods/Plots') have been used internally for the sake of providing applied research results. If the focus of the interested reader is on the queue model, please ignore the rest of the folders of this project.

### Basics on the batch-service queue model for Blockchain ###

Transactions arrive at the system following a Poisson distribution with parameter \lambda. Once a block is filled with transactions or the block timer expires, a block is started to be mined, which mining time follows an exponential random variable with parameter \mu. The effect of forks is considered by re-adding the transactions involved in a fork, subject to its occurrence (with p_fork probability).

![picture](Resources/batch_service_queue.png)

### How to use? ###

1) Go to the "Matlab Code" folder and add it to the path 

2) Execute the file 'Example.m' in the './Matlab code' folder as an example

3) Gather the logs provided by the simulation:

![picture](Resources/example_simulation.png)

### Validation ###

The model has been validated with the batch-service queue simulator provided in [https://github.com/fwilhelmi/batch_service_queue_simulator](https://github.com/fwilhelmi/batch_service_queue_simulator). 

1) Results for T_w = 0.1 s (forks disabled):

![picture](Resources/total_delay_forks0_tw01.png)

2) Results for T_w = 2 s (forks disabled):

![picture](Resources/total_delay_forks0_tw2.png)

3) Results for T_w = 0.1 s (forks enabled):

![picture](Resources/total_delay_forks1_tw2.png)

4) Results for T_w = 2 s (forks enabled):

![picture](Resources/total_delay_forks1_tw05.png)

### Want to contribute? ###

If you want to contribute, please contact fwilhelmi@cttc.cat