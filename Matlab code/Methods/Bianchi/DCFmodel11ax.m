%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [throughput_stas,throughput_ap]=DCFmodel11ax(nAps, nStas, MCS, isCwFixed)

    %load('constants.mat')   
    
    % Generic MAC modeling constants
    PAYLOAD_LENGTH = 12000;             % Payload length in bits
    CW_FIXED = 32;                      % CW value used in case of having a fixed exponential backoff procedure
    CW_MIN_AP = 16;
    CW_MAX_STAGE_AP = 5;
    CW_MIN_STA = 32;
    CW_MAX_STAGE_STA = 5;
    NUM_PACKETS_AGGREGATED = 1;
    CCA_THRESHOLD = -82; % Minimum sensitivity (in dBm) to assess signals generating contention

    % 802.11ax MAC constants
    IEEE_AX_PHY_HE_SU_DURATION = 100e-6;
    IEEE_AX_MD_LENGTH = 32;
    IEEE_AX_MH_LENGTH = 320;
    IEEE_AX_OFDM_SYMBOL_GI32_DURATION = 16e-6;
    IEEE_AX_MAX_PPDU_DURATION = 5484e-6;

    % DCF 
    DIFS = 34E-6; 
    SIFS = 16E-6;
    Te = 9E-6;
    % RTS/CTS 
    L_RTS = 160;
    L_CTS = 112;
    % Service Field  
    L_SF = 16;
    % MPDU Delimiter if PA is used
    L_DEL=32;
    %MAC Header including FCS
    L_MACH = 272; %320
    % Tail bits
    L_TAIL = 6;
    % ACK/Block ACK    
    L_BACK = 240; %L_ACK = 112;

    SUSS = 1;
    % Physical Header (HE SU Format)
    LEGACY_PHYH = 20E-6;
    %HE_PHYH = (16 + SUSS*4)*1E-6;
    HE_PHYH = (16 + SUSS*16)*1E-6;
    % Duration of OFDM symbol (CP of 3.2us is included)
    T_OFDM = 4E-6;
  
    L = PAYLOAD_LENGTH;
    W = 20e6;
    Na = NUM_PACKETS_AGGREGATED;

    Nsc_list = [26 52 102 234 468 980 1960];
    W_list = [2.5 5 10 20 40 80 160];
    Ym_list = [1 2 2 4 4 6 6 6 8 8 10 10];
    Yc_list = [1/2 1/2 3/4 1/2 3/4 1/2 2/3 3/4 3/4 5/6 3/4 5/6];

    % Num of spatial streams at each STA (based on the number of antennas at STAs and AP)
    SUSS = 1;

    % AP parameters
    ap.CWmin=CW_MIN_AP;             % Minimum Value of Contendion Window
    ap.MaxStage=CW_MAX_STAGE_AP;    % Number of stages for backoff
    ap.backoff_stage = 0;           % Initial backoff stage
    ap.backoff_counter = floor((2^(ap.backoff_stage)*ap.CWmin)*rand()); % Values [0,X-1]
    ap.tx_packets = 0;
    ap.succ_tx_packets = 0;
    ap.collisions = 0;

    % Iterate for each STA
    for i=1:nStas
        sta(i).CWmin=CW_MIN_STA;
        sta(i).MaxStage=CW_MAX_STAGE_AP;
        sta(i).backoff_stage = 0;
        sta(i).backoff_counter = floor((2^(sta(i).backoff_stage)*sta(i).CWmin)*rand());
        sta(i).tx_packets = 0;
        sta(i).succ_tx_packets = 0;    
        sta(i).collisions = 0;
    end
    
    %[T_s,~,T_c] = SUtransmission80211ax(L,Na,W,SUSS,MCS,0);  % Calculate the duration of a successful and a collision slot
    %[Nsc,Ym,Yc]=PHYParams80211ax(W,MCS); % Load PHY Parameters
    Nsc = Nsc_list(find(W_list == 20));
    Ym = Ym_list(MCS);
    Yc = Yc_list(MCS);
    if MCS == -1
        T_s = 0;
        limited_num_packets_aggregated = 0;
        T_c = 0;
    else
        bits_ofdm_sym_legacy = 24;
        bits_ofdm_sym = Nsc * Ym * Yc * SUSS;
        % Rate = Nsc * Ym * Yc * SUSS;
        % Rate_20MHz = 52 * Ym * Yc; % In legacy mode
        % Duplicate RTS/CTS for bandwidth allocation
        T_RTS  = LEGACY_PHYH + ceil((L_SF+L_RTS)/bits_ofdm_sym_legacy)*T_OFDM;
        T_CTS  = LEGACY_PHYH + ceil((L_SF+L_CTS)/bits_ofdm_sym_legacy)*T_OFDM;
        % Data
        limited_num_packets_aggregated = Na;
        while (limited_num_packets_aggregated > 0)
            T_DATA = IEEE_AX_PHY_HE_SU_DURATION + ceil((L_SF + limited_num_packets_aggregated ...
                * (IEEE_AX_MD_LENGTH + IEEE_AX_MH_LENGTH + L)) / bits_ofdm_sym) * IEEE_AX_OFDM_SYMBOL_GI32_DURATION;
            if(T_DATA <= IEEE_AX_MAX_PPDU_DURATION) 
                break;
            else
                limited_num_packets_aggregated = limited_num_packets_aggregated - 1;
            end
        end
        % Block ACK
        T_BACK = 32e-6; %Legacy_PHYH + ceil((L_SF+L_BACK+L_TAIL)/Rate_20MHz)*T_OFDM;
        % Successful slot
        T_s = T_RTS + SIFS + T_CTS + SIFS + T_DATA + SIFS + T_BACK + DIFS + Te; % (Implicit BACK request)
        % Collision slot
        T_c = T_RTS + SIFS + T_CTS + DIFS + Te;
    end
    T_e = 9e-6;

    if(isCwFixed) 
        tau = 2/(CW_FIXED+1);
        p_e = (1-tau)^(nAps+nStas);
        p_s = (nAps+nStas) * tau * (1-tau)^((nAps+nStas)-1);
        p_c = 1-p_e-p_s;
        % Throughput
        throughput_ap = (p_s*L) / (p_e*T_e + p_s*T_s + p_c*T_c);
        throughput_stas = throughput_ap;
    else 
        % Number of model iterations (for convergence)
        max_it = 1000;
        % Model Init parameters:
        tau_aps = zeros(1,max_it);
        tau_stas = zeros(1,max_it);
        tau_aps(1) = 2/(CW_MIN_AP+1);
        tau_stas(1) = 2/(CW_MIN_STA+1);
        for i=1:max_it   
            % AP collision prob
            p_aps = 1 - nAps*tau_aps(i)*(1-tau_aps(i))^(nAps-1) * ...
                (1-tau_stas(i))^(nStas);          
            %p_aps = 1-(1-tau_aps(i))^(nAps-1)*(1-tau_stas(i))^(nStas);
            % STAs collision probability
            p_stas = 1 - nStas*tau_stas(i)*(1-tau_stas(i))^(nStas-1) * ...
                (1-tau_aps(i))^(nAps);      
            %p_stas = 1-(1-tau_stas(i))^(nStas-1)*(1-tau_aps(i))^(nAps);
            % AP transmission probability
            a_ap= 1-p_aps-p_aps*(2*p_aps)^CW_MAX_STAGE_AP;
            b_ap= 1-2*p_aps;
            EB_aps = ((a_ap)/(b_ap))*(CW_MIN_AP/2) - (1/2);
            tau_aps(i) = 1/(EB_aps+1);
            % STA transmission probability
            a_sta= 1-p_stas-p_stas*(2*p_stas)^CW_MAX_STAGE_STA;
            b_sta= 1-2*p_stas;
            EB_stas = ((a_sta)/(b_sta))*(CW_MIN_STA/2) - (1/2);
            tau_stas(i) = 1/(EB_stas+1);             
            if(i>4)
                tau_stas(i+1)=mean(tau_stas(1:i));
                tau_aps(i+1)=mean(tau_aps(1:i));
            end
        end
        % Slots probabilities
        p_e = (1-tau_aps(max_it))^nAps*(1-tau_stas(max_it))^nStas;
        p_s_aps = nAps*tau_aps(max_it)*(1-tau_aps(max_it))^(nAps-1)*(1-tau_stas(max_it))^nStas;
        p_s_stas = nStas*tau_stas(max_it)*(1-tau_aps(max_it))^(nAps)*(1-tau_stas(max_it))^(nStas-1);
        p_c = 1-p_e-p_s_aps-p_s_stas;
        % Throughput
        throughput_ap = p_s_aps*L/(p_e*T_e + p_s_aps*T_s + p_c*T_s);
        throughput_stas = p_s_stas*L/(p_e*T_e + p_s_stas*T_s + p_c*T_s);
    end
    
end