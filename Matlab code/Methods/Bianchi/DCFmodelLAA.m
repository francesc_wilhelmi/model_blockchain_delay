%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [throughput_stas,throughput_ap]=DCFmodelLAA(nAps, nStas, MCS, isCwFixed)
    
    % Generic MAC modeling constants
    PAYLOAD_LENGTH = 2048*8;            % Payload length in bits
    CW_FIXED = 16;                      % CW value used in case of having a fixed exponential backoff procedure
    CW_MIN_AP = 16;
    CW_MAX_STAGE_AP = 2;
    CW_MIN_STA = 16;
    CW_MAX_STAGE_STA = 2;
    NUM_PACKETS_AGGREGATED = 1;

    % LAA constants
    T_OFDM = 4E-6; % Duration of OFDM symbol (CP of 3.2us is included)
    LAA_OFDM_SYMBOL_GI32_DURATION = 16e-6;
    SLOT_LTE = 1e-3;
    LAA_MAX_PPDU_DURATION = 8e-3; % 10-3 if there is coexistence
    D_LTE = 5e-4; % delay for next transmission 
    SUSS = 1; % Num of spatial streams at each STA (based on the number of antennas at STAs and AP)    
    DATA_SYMBOLS_PER_SLOT = 11;
    TOTAL_SYMBOLS_PER_SLOT = 14;
    
    W = 20e6;
    Na = NUM_PACKETS_AGGREGATED;
    
    % DCF 
    DIFS = 43-6; % Access priority class #3
    SIFS = 0;
    Te = 9E-6;  % For the backoff, T_s = 9e-6 (as in WiFi)

    % MCS
    Nsc_list = [12 72 180 300 600 900 1200]; % e.g., 12 sub-carriers (each with 14 OFDM symbols = 168 REs)
    W_list = [0.18 1.4 3 5 10 15 20]; % from 180 kHz to 20 MHz
    Ym_list = [1 2 2 4 4 6 6 6 8 8 10 10];
    Yc_list = [1/2 1/2 3/4 1/2 3/4 1/2 2/3 3/4 3/4 5/6 3/4 5/6];
    % Minimum unit of radio resource: 180 kHz bandwidth over a TTI equal to one subframe
    % Maximum unit of radio resource: 20 MHz bandwidth, with 100 RBs available (=1200 subcarriers)
    % Minimum resolution of data tx: 1 subframe (1 ms)

    % Compute slot times
    Nsc = Nsc_list(find(W_list == 20));
    Ym = Ym_list(MCS);
    Yc = Yc_list(MCS);
    if MCS == -1
        T_s = 0;
        limited_num_packets_aggregated = 0;
        T_c = 0;
    else        
        T_s = LAA_MAX_PPDU_DURATION + D_LTE; % Successful slot        
        T_c = T_s; % Collision slot                         
    end
    bits_ofdm_sym = Nsc * Ym * Yc * SUSS;
    n_slots = LAA_MAX_PPDU_DURATION/SLOT_LTE;
    L = n_slots*bits_ofdm_sym*DATA_SYMBOLS_PER_SLOT;
    
    if(isCwFixed) 
        tau = 2/(CW_FIXED+1);
        p_e = (1-tau)^(nAps+nStas);
        p_s = (nAps+nStas) * tau * (1-tau)^((nAps+nStas)-1);
        p_c = 1-p_e-p_s;
        % Throughput
        throughput_ap = Rate*1e6 * (p_s*T_s) / (p_e*T_e + p_s*T_s + p_c*T_c);
        throughput_stas = throughput_ap;
    else 
        % Number of model iterations (for convergence)
        max_it = 1000;
        % Model Init parameters:
        tau_aps = zeros(1,max_it);
        tau_stas = zeros(1,max_it);
        tau_aps(1) = 2/(CW_MIN_AP+1);
        tau_stas(1) = 2/(CW_MIN_STA+1);
        for i=1:max_it   
            % AP collision prob
            p_aps = 1 - nAps*tau_aps(i)*(1-tau_aps(i))^(nAps-1) * ...
                (1-tau_stas(i))^(nStas);          
            % STAs collision probability
            p_stas = 1 - nStas*tau_stas(i)*(1-tau_stas(i))^(nStas-1) * ...
                (1-tau_aps(i))^(nAps);  
            % AP transmission probability
            a_ap= 1-p_aps-p_aps*(2*p_aps)^CW_MAX_STAGE_AP;
            b_ap= 1-2*p_aps;
            EB_aps = ((a_ap)/(b_ap))*(CW_MIN_AP/2) - (1/2);
            tau_aps(i) = 1/(EB_aps+1);
            % STA transmission probability
            a_sta= 1-p_stas-p_stas*(2*p_stas)^CW_MAX_STAGE_STA;
            b_sta= 1-2*p_stas;
            EB_stas = ((a_sta)/(b_sta))*(CW_MIN_STA/2) - (1/2);
            tau_stas(i) = 1/(EB_stas+1);             
            if(i>4)
                tau_stas(i+1)=mean(tau_stas(1:i));
                tau_aps(i+1)=mean(tau_aps(1:i));
            end
        end
        % Slots probabilities
        p_e = (1-tau_aps(max_it))^nAps*(1-tau_stas(max_it))^nStas;
        p_s_aps = nAps*tau_aps(max_it)*(1-tau_aps(max_it))^(nAps-1)*(1-tau_stas(max_it))^nStas;
        p_s_stas = nStas*tau_stas(max_it)*(1-tau_aps(max_it))^(nAps)*(1-tau_stas(max_it))^(nStas-1);
        p_c = 1-p_e-p_s_aps-p_s_stas;
        % Throughput
        %throughput_ap = (10/14)*p_s_aps*L/(p_e*T_e + p_s_aps*T_s + p_c*T_s);
        throughput_ap = (DATA_SYMBOLS_PER_SLOT/TOTAL_SYMBOLS_PER_SLOT)*p_s_aps*L / (p_e*T_e + p_s_aps*T_s + p_c*T_c);
        throughput_stas = (DATA_SYMBOLS_PER_SLOT/TOTAL_SYMBOLS_PER_SLOT*p_s_stas*L/(p_e*T_e + p_s_stas*T_s + p_c*T_s);
    end
    
end