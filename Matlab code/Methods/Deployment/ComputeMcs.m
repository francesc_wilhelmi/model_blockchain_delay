%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [McsMatrixAps, McsMatrixStas] = ComputeMcs(deployment)
% mcs_per_device computes the allowed Modulation Coding Scheme for each device
% INPUT:
%   * deployment: object containing information about the deployment
% OUTPUT:
%   * McsMatrixAps: MCS to be used for inter-AP communications 
%     (rows: number of channels, columns: MCS index)
%   * McsMatrixStas: MCS to be used for AP-STA communications
%     (rows: number of channels, columns: MCS index)

    load('constants.mat');
    load('conf_environment.mat');

    McsMatrixAps = cell(1,deployment.nAps);    
    McsMatrixStas = cell(1,deployment.nAps);
    
    for ap_ix = 1 : deployment.nAps
                
        %%% Check links between APs
       
        McsMatrixAps{ap_ix} = zeros(deployment.nAps, NUM_CHANNELS_SYSTEM);
        
        for ap_ix_2 = 1 : deployment.nAps
        
            for ch_ix = 1 : (log2(NUM_CHANNELS_SYSTEM) + 1) 	% For 1, 2, 4 and 8 channels

                if deployment.signalApAp(ap_ix, ap_ix_2) < -82 +((ch_ix-1)*3) 
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_FORBIDDEN;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -82 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -79 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_BPSK_1_2;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -79 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -77 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_QPSK_1_2;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -77 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -74 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_QPSK_3_4;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -74 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -70 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_16QAM_1_2;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -70 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -66 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_16QAM_3_4;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -66 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -65 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_64QAM_2_3;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -65 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -64 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_64QAM_3_4;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -64 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -59 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_64QAM_5_6;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -59 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -57 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_256QAM_3_4;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -57 + ((ch_ix-1)*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -54 +((ch_ix-1)*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_256QAM_5_6;
                elseif (deployment.signalApAp(ap_ix, ap_ix_2) >= -54 + (ch_ix*3) && deployment.signalApAp(ap_ix, ap_ix_2) < -52 +(ch_ix*3))
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_1024QAM_3_4;
                else
                    McsMatrixAps{ap_ix}(ap_ix_2, ch_ix) = MODULATION_1024QAM_5_6;
                end

            end
        
        end
        
        %%% Check links between APs and STAs
        
        McsMatrixStas{ap_ix} = zeros(deployment.nStas, NUM_CHANNELS_SYSTEM);
       
        for sta_ix = 1 : deployment.nStas
        
            for ch_ix = 1 : (log2(NUM_CHANNELS_SYSTEM) + 1) 	% For 1, 2, 4 and 8 channels

                if deployment.signalApSta(ap_ix, sta_ix) < -82 +((ch_ix-1)*3) 
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_FORBIDDEN;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -82 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -79 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_BPSK_1_2;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -79 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -77 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_QPSK_1_2;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -77 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -74 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_QPSK_3_4;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -74 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -70 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_16QAM_1_2;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -70 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -66 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_16QAM_3_4;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -66 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -65 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_64QAM_2_3;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -65 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -64 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_64QAM_3_4;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -64 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -59 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_64QAM_5_6;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -59 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -57 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_256QAM_3_4;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -57 + ((ch_ix-1)*3) && deployment.signalApSta(ap_ix, sta_ix) < -54 +((ch_ix-1)*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_256QAM_5_6;
                elseif (deployment.signalApSta(ap_ix, sta_ix) >= -54 + (ch_ix*3) && deployment.signalApSta(ap_ix, sta_ix) < -52 +(ch_ix*3))
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_1024QAM_3_4;
                else
                    McsMatrixStas{ap_ix}(sta_ix, ch_ix) = MODULATION_1024QAM_5_6;
                end

            end
        
        end
    
    end
           
end