%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [] = WriteDeploymentDetails(LOGS_ENABLED, logs_file, deployment)

    load('constants.mat')
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL1 'Deployment details:']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL2 'Number of APs/BSs: ' num2str(deployment.nAps)]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Number of STAs: ' num2str(deployment.nStas)]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Max. distance between APs: ' num2str(max(max(deployment.distApAp))) ' m']);
    max_d = 0;
    min_rx = 0;
    for i = 1 : deployment.nStas
        if deployment.distApSta(deployment.staNearestAp(i),i) > max_d
            max_d = deployment.distApSta(deployment.staNearestAp(i),i);
        end 
        if deployment.signalApSta(deployment.staNearestAp(i),i) < min_rx
            min_rx = deployment.signalApSta(deployment.staNearestAp(i),i);
        end 
    end
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Max. distance between STAs and the nearest AP: ' num2str(max_d) ' m']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Min. rx signal at STAs from the nearest AP: ' num2str(min_rx) ' dBm']);

end

