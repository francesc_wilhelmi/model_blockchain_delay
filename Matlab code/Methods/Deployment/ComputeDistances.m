%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [distApAp, distApSta, distStaSta] = ComputeDistances(deployment)
%ComputeDistances computes the distance between any pair of devices in a
% wireless deployment
% in a wireless deployment
%   INPUT
%   - deployment: object containing the information of the deployment
%   OUTPUT
%   - distApAp: distance between APs
%   - distApSta: distance between APs and STAs
%   - distStaSta: distance between STAs
 
    % Distance between APs and APs/STAs
    for i = 1 : deployment.nAps

        for j = 1 : deployment.nAps
            distApAp(i,j) = sqrt((deployment.ApPos(i,1)-deployment.ApPos(j,1))^2 ...
                + (deployment.ApPos(i,2) - deployment.ApPos(j,2))^2);
        end

        for n = 1 : deployment.nStas
            distApSta(i,n) = sqrt((deployment.ApPos(i,1)-deployment.StaPos(n,1))^2 ...
                + (deployment.ApPos(i,2) - deployment.StaPos(n,2))^2);
        end   

    end
    % Distance between STAs
    for i = 1 : deployment.nStas   
        for n = 1 : deployment.nStas
            distStaSta(i,n) = sqrt((deployment.StaPos(i,1)-deployment.StaPos(n,1))^2 ...
                + (deployment.StaPos(i,2) - deployment.StaPos(n,2))^2);
        end       
    end

end