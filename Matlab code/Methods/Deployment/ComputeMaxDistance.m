%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function max_distance = ComputeMaxDistance(deployment)
%ComputeMaxDistance computes the maximum distance at which an STA can be
% placed wrt to its closest AP
% in a wireless deployment
%   INPUT
%   - deployment: object containing the information of the deployment
%   OUTPUT
%   - max_distance: value indicating the distance in meters

    PLd1=5;           % Path-loss factor
    shadowing = 9.5;  % Shadowing factor
    obstacles = 30;   % Obstacles factor
    alfa = 4.4;
    
    d = 0;
    delta_d = .1;
    while (true)        
        rx_power = deployment.TxPower - ...
            (PLd1 + 10 * alfa * log10(d) + ...
            shadowing / 2 + (d/10) .* obstacles / 2);
        
        if rx_power < deployment.ccaThreshold
            max_distance = d;
            break;
        else
            d = d + delta_d;
        end
    end   	

end