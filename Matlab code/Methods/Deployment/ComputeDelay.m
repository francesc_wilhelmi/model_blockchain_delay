%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [delay] = ComputeDelay(deployment, technology, link_type, node_id, data_length, INTERFERENCE_MODE)
% ComputeDelay computes the delay of a given transmission given the type of 
% deployment (e.g., WiFi, NR), the type of link (mesh, sidelink) and the 
% current status of the network
% INPUT:
%   * deployment: object containing information about the deployment
%   * technology: underlying technology (WiFi vs NR)
%   * link_type: type of link considered
% OUTPUT:
%   * McsMatrixAps: MCS to be used for inter-AP communications 
%     (rows: number of channels, columns: MCS index)
%   * McsMatrixStas: MCS to be used for AP-STA communications
%     (rows: number of channels, columns: MCS index)
     
    switch technology
        
        % WiFi
        case 1 %WIFI
            
             if link_type == 1 %ACCESS_NETWORK                 
                [throughput_sta,~] = ComputeThroughput(deployment, 1, INTERFERENCE_MODE);
                delay = data_length / throughput_sta(node_id);                 
             elseif link_type == 2 %P2P_NETWORK             
                [~,throughput_ap] = ComputeThroughput(deployment, 2, INTERFERENCE_MODE);
                % Compute the expected number of hops (approximation)
                average_number_of_hops = deployment.nAps / mean(sum(deployment.signalApAp>deployment.ccaThreshold));
                % Compute the delay
                delay = average_number_of_hops * data_length / mean(throughput_ap);                                     
             end
        
        % NR
        case 2 %NR
            
            if link_type == 1       % ACCESS_NETWORK
                delay = 0.1;
            elseif link_type == 2   % P2P_NETWORK
                delay = 0.01;
            end
        
        % Unknown    
        otherwise
            disp(['Unknown technology type. Current available technologies: WIFI (' num2str(1) ') and NR (' num2str(2) ')'])
       
    end   
    
end

