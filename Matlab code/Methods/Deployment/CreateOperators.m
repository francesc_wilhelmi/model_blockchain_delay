%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

function [deployment, operators] = CreateOperators(deployment)
% CreateOperators creates opeators to serve users in a wireless deployment
% INPUT:
%   * deployment: object containing information about the deployment
% OUTPUT:
%   * deployment: updated deployment object
%   * operators: object containing information about the operators

    conf_deployment

    % Define APs' ownership
    aps_indices_random = randperm(deployment.nAps);
    n = 1;
    for i = 1 : nOperators
        operators(i).operator_id = i;
        numAps = deployment.nAps/nOperators; % operators(i).ownershipRatio
        if i < nOperators
            operators(i).ownedAps = aps_indices_random(n:n+numAps);
        else
            operators(i).ownedAps = aps_indices_random(n:end);
        end
        n = n+numAps;
    end
    
    % Allocate frequency resources
    %    - WiFi: different operators CAN use the same freq. resources
    %    - Cellular: different operators CANNOT use the same freq. resources
    switch planning_mode
        
        %   - Channels are allocated randomly
        case 1  %WIFI_SINGLE_CHANNEL_RANDOM
            
            for i = 1 : nOperators
                operators(i).channelRange = 1 : NUM_CHANNELS_SYSTEM;
                operators(i).channelPerAp = zeros(1,deployment.nAps);
                for j = 1 : length(operators(i).ownedAps)
                    selected_channel = operators(i).channelRange(randi(length(operators(i).channelRange)));
                    operators(i).channelPerAp(operators(i).ownedAps(j)) = selected_channel;
                end
            end
        %   - The same channel is allocated to all the APs               
        case 2 %WIFI_SINGLE_CHANNEL_ALL_SAME
            
            for i = 1 : nOperators
                operators(i).channelRange = 1;
                selected_channel = 1;
                for j = 1 : length(operators(i).ownedAps)
                    operators(i).channelPerAp(operators(i).ownedAps(j)) = selected_channel;
                end
            end
        %   - Unknown case (display error message)
        otherwise
            disp('Error: An unknown resource planning mode was introduced');
           
    end
    
    % Update deployment with new information
    for i = 1 : deployment.nAps
        for o = 1 : length(operators)
            if sum(find(operators(o).ownedAps==i))
                deployment.operatorAps(i) = o;
                deployment.channelAps(i) = operators(o).channelPerAp(i);
            end
        end
    end
    
end

