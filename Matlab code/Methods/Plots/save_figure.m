%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

% FILE DESCRIPTION: this method saves a figure in different formats

function [] = save_figure( fig, fig_name, path_figure )
%   INPUT: 
%       * fig - figure to be saved
%       * fig_name - desired name for the figure
%       * path_figure - desired path to save the figure

    savefig([path_figure fig_name '.fig'])
    saveas(gcf, [path_figure fig_name], 'png')
    %saveas(gcf, [path_figure fig_name], 'epsc')  
    
end

