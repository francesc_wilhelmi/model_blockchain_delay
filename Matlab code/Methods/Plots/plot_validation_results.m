%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

% FILE DESCRIPTION: this method plots additional results for validating the
% batch service queue model with simulations

function [] = plot_validation_results()

    % Load model and simulation results
    load('model_output')
    load('simulator_output')

    % Queue delay
    for t = 1 : length(timeout)
        fig = figure;
        colors_plot = {'b','k','r','g','c','m','y'};
        lgd = cell(size(mean_delay{t},1),1) ;
        aux = 1;
        for i = 1 : size(mean_delay{t},1)
            plot(mean_delay{t}(i,:), [colors_plot{i} '-.'], 'linewidth', 2.0)
            lgd{aux} = strcat('sim: \lambda=',num2str(lambda(i)));
            hold on
            plot(T_queue{t}(i,:), [colors_plot{i} 's'], 'markersize', 10.0)
            lgd{aux+1} = strcat('model: \lambda=',num2str(lambda(i)));
            aux = aux+2;
        end
        legend(lgd, 'numcolumns', 3)
        xticks(1:10)
        xticklabels(3000.*(1:10))
        grid on
        grid minor
        xlabel('Block size, S^B (bits)')
        ylabel('Total delay (s)')
        title(['Timer = ' num2str(timeout(t)) ' s'])
        set(gca,'fontsize',16)
        axis([1 length(block_size) 0 1.1*max(max(max(mean_delay{t})),max(max(T_queue{t})))])
    end
    save_figure(fig, 'delay_sim_vs_model', '')

    % Fork probability
    if FORKS_ENABLED
        fig = figure;
        colors_plot = {'b','k','r','g','c','m','y'};
        lgd = cell(size(p_fork_sim,1),1) ;
        aux = 1;
        for i = 1 : size(p_fork_sim,1)
            plot(p_fork_sim(i,:), [colors_plot{i} '-.'], 'linewidth', 2.0)
            lgd{aux} = strcat('sim: \lambda=',num2str(lambda(i)));
            hold on
            plot(p_fork(i,:), [colors_plot{i} 's'], 'markersize', 10.0)
            lgd{aux+1} = strcat('model: \lambda=',num2str(lambda(i)));
            aux = aux+2;
        end
        legend(lgd, 'numcolumns', 3)
        xticks(1:10)
        xticklabels(3000.*(1:10))
        grid on
        grid minor
        xlabel('Block size, S^B (bits)')
        ylabel('Fork probability')
        set(gca,'fontsize',16)
        axis([1 length(block_size) 0 1.1*max(max(max(p_fork_sim)),max(max(p_fork)))])
    end

end