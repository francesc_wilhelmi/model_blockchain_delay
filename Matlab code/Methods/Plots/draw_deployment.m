%%% *********************************************************************
%%% * Batch-service queue model for Blockchain                          *
%%% * By: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)     *
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * Repo.: bitbucket.org/francesc_wilhelmi/model_blockchain_delay     *
%%% *********************************************************************

% FILE DESCRIPTION: this method plots the deployment used for obtaining
% results from both the model and the simulator

function [] = draw_deployment(deployment)
%   INPUT: 
%       * deployment - deployment object containing the location of nodes
%       and other information

    load('conf_deployment.mat');
    margin = 1;
    MinX=min([min(deployment.ApPos(:,1)),min(deployment.StaPos(:,1))]) - margin;
    MinY=min([min(deployment.ApPos(:,2)),min(deployment.StaPos(:,2))]) - margin;
    MaxX=max([max(deployment.ApPos(:,1)),max(deployment.StaPos(:,1))]) + margin;
    MaxY=max([max(deployment.ApPos(:,2)),max(deployment.StaPos(:,2))]) + margin; 
    fig=figure;
    axes;
    scatter(deployment.ApPos(:,1), deployment.ApPos(:,2), 90, [0 0 0], 'filled');
    hold on;     
    scatter(deployment.StaPos(:,1), deployment.StaPos(:,2), 60, [0 0 0], 'filled', 'Marker', 'x', 'MarkerEdgeColor', 'r');
    for i = 1 : deployment.nAps
        hex_pgon = nsidedpoly(6,'Center',[deployment.ApPos(i,1) deployment.ApPos(i,2)],'SideLength',R);
        plot(hex_pgon);
        text(deployment.ApPos(i,1),deployment.ApPos(i,2)+1,['Ap' num2str(i)],'fontsize',14,'horizontal','left','vertical','bottom') 
    end    
    %line([deployment(i).x, wlan(i).xn], [deployment(i).y, wlan(i).yn], [deployment(i).z, wlan(i).zn], 'Color', [0.4, 0.4, 1.0], 'LineStyle', ':');
    xlabel('x [meters]','fontsize',14);
    ylabel('y [meters]','fontsize',14);
    axis([0-(max(abs(MinX),abs(MaxX))) (max(abs(MinX),abs(MaxX))) ...
        0-(max(abs(MinY),abs(MaxY))) (max(abs(MinY),abs(MaxY)))])
    legend({'AP','STA'})
    grid on
    grid minor
    set(gca,'fontsize',18);
    save_figure( fig, './output/random_deployment', '' )
    
end